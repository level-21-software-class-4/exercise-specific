use LibraryDb
go
select * from Student			/*学生表*/
select * from BorrowRecord		/*学生借阅信息*/
select * from StudentExtInfo	/*学生扩展信息表*/
select * from BookInfo			/*图书信息表*/
select * from BookExtInfo		/*图书类别表*/

go
--1. 利用另外一个文件initDb.sql，完成以下题目:
--1.1. (10分) 对学生姓名创建普通索引(idx_StudentName )
--CREATE UNIQUE INDEX index_name ON table_name (column_name)
	create  nonclustered index idx_StudentName on Student(StudentName);
--1.2. (10分) 创建视图V_StudentHobbyInfo ,其中包含姓名(StudentName),爱好
--(Hobby),特长(Specility)列，并分别取别名为
--vw_StudentName,vw_Hoppy,vw_Speclitiy
	--create view <视图名> [(<列名>[,<列名>]...)]
	--as <子查询> [with check option]
	create view V_StudentHobbyInfo 
	as
	 select S.StudentName,E.Hobby,E.Speciality from Student S
	 join StudentExtInfo E on S.StudentCode = E.StudentCode
	 go
	 select * from V_StudentHobbyInfo;

--1.3. (5分) 查询StudentInfo表，以每页3条记录，第2页的记录展示
	select top 3 * from Student where StudentCode not in (
	select top 3 StudentCode from Student)
--1.4. (5分) 查询学生马又云的生源地，要求显示：姓名(StudentNameame)，生源地
--(OriginPosition)
	--select * from Student S
	--join StudentExtInfo E on S.StudentCode = E.StudentCode

	create proc p_name
	@n varchar(20) output
	as
	begin
	select S.StudentName,E.OriginPosition from Student S
	 join StudentExtInfo E on S.StudentCode = E.StudentCode
	 where S.StudentName = @n
	end

	go
	declare @n varchar(20) = '马又云'
	exec p_name @n


	
--1.5. (5分) 查询学生表(StudentInfo)中还未还书的读者的学号(StudentCode)和姓
--名(StudentName),以及借书时间(BorrowTime)
	create view v_b
	as
	select S.StudentCode,S.StudentName,B.BorrowTime from Student S 
	join BorrowRecord B on S.StudentCode = B.StudentCode
	where B.ReturnTime is null
	go
	drop view v_b
	select * from v_b
	go